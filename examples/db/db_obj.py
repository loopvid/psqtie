#! /usr/bin/env python3
'''Common psqtie.DB() object for other submodules'''
# pylint: disable = missing-docstring, global-statement

import os
from contextlib import closing

from psqtie import DB as BaseDB


class DB(BaseDB):
    '''DB'''
    _db_name = os.environ.get("PSQTIE_DB", "test")
    _user = os.environ.get("PSQTIE_USER", "test")
    _password = os.environ.get("PSQTIE_PASSWORD", None)
    _host = os.environ.get("PSQTIE_HOST", None)
    _port = os.environ.get("PSQTIE_PORT", None)


# will be created at the end of this file
db = None


def disable_auto_commit():
    global db

    if db and not db.auto_commit:
        # nothing to do
        return

    db = DB(auto_commit=False)


def reenable_auto_commit():
    global db

    if db and db.auto_commit:
        # nothing to do
        return

    # Make sure the connection is closed, so it will be properly garbage
    # collected. Note that it isn't a problem to call close() on an already
    # closed connection.
    if db:
        db.close()

    db = DB(auto_commit=True)


def execute_transaction(*statements_and_values):
    '''Execute several queries in the same transaction.

    statements_and_values should be any number of (statement, values) pairs to
    be executed within the transaction. For example:

    execute_transaction(
        (
            'INSERT INTO mytable (col1, col2) VALUES (%(col1)s, %(col2)s)',
            {'col1': 1, 'col2': 2}
        ),
        (
            'UPDATE mytable2 SET col3=%s WHERE col4=%s',
            [3, 4]
        )
    )
    '''
    with closing(DB(auto_commit=False)) as _db:
        for statement, values in statements_and_values:
            _db.execute(statement, values)
        _db.commit()


# expose psqtie.DB's methods as public functions, so we can replace the DB
# object as needed and immediately affect them all without any further action by
# modules importing db

def query(*args, **kwargs):
    return db.query(*args, **kwargs)


def fetch(*args, **kwargs):
    return db.fetch(*args, **kwargs)


def fetchall(*args, **kwargs):
    return db.fetchall(*args, **kwargs)


def fetchone(*args, **kwargs):
    return db.fetchone(*args, **kwargs)


def execute(*args, **kwargs):
    return db.execute(*args, **kwargs)


def select_from(*args, **kwargs):
    return db.select_from(*args, **kwargs)


def insert_into(*args, **kwargs):
    return db.insert_into(*args, **kwargs)


def update(*args, **kwargs):
    return db.update(*args, **kwargs)


def delete_from(*args, **kwargs):
    return db.delete_from(*args, **kwargs)


def commit():
    db.commit()


def rollback():
    db.rollback()


def close():
    db.close()


# static methods
row2dict = DB.row2dict


# create initial db object with auto_commit enabled
reenable_auto_commit()
