#! /usr/bin/env python3
'''Convenience module for centralized DB access.

This module instantiates a single psqtie.DB() object using environment variables
to determine the DB's address, port, user and password. This allows other modules
to simply import this one and start querying without going through the trouble
of instantiating a DB object themselves. For example:

$ export PSQTIE_DB="mydb"
$ export PSQTIE_USER="myuser"

from myproject import db

db.insert_into('mytable', {'col1': 'value1', 'col2': 'value2'})

qty = db.fetchone('select count(*) as qty from mytable')['qty']
'''

from .db_obj import *
