import setuptools

with open('README.md', 'r', encoding='utf-8') as _f:
    long_description = _f.read()

extras = {
    'postgresql': ['psycopg2>=2.8.2']
}

setuptools.setup(
    name="psqtie",
    version="1.0.0",
    author="loopvid",
    description="A small convenience wrapper class for psycopg2 and sqlite3",
    long_description=long_description,
    url="https://gitlab.com/loopvid/psqtie",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: CC0 1.0 Universal (CC0 1.0) Public Domain Dedication",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    extras_require=extras,
)
