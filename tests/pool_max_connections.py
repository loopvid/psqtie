#! /usr/bin/env python3
'''Check that connections are being properly freed up by making more
concurrent queries than there are connections available in the pool.'''
# pylint: disable = redefined-outer-name, global-statement

import sys
import threading

from _test_db import PooledPostgresTestDB


verbose = False
threads_done = 0
total = 200


def run_a_pool_query(db):
    '''Run a query using the pool.'''
    global threads_done

    statement = 'SELECT COUNT(*) FROM test'
    db.fetchone(statement)

    threads_done += 1

    if verbose:
        print('\r{}/{}  '.format(threads_done, total), end='',
              file=sys.stderr)

def test_pool_threading(db_class):
    '''Check that connections are being properly freed up by making more
    concurrent queries than there are connections available in the pool.'''
    # pylint: disable = unused-variable

    global threads_done

    threads_done = 0

    db = db_class()

    for i in range(total):
        t = threading.Thread(target=run_a_pool_query, args=[db], daemon=True)
        t.start()

    # wait for all threads to finish
    while threads_done < total:
        for t in threading.enumerate():
            if t == threading.current_thread():
                continue
            else:
                t.join()

    if verbose:
        print(file=sys.stderr)


if __name__ == '__main__':
    for db_class in [PooledPostgresTestDB]:
        test_pool_threading(db_class)
