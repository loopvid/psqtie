#! /usr/bin/env python3
'''Test convenience methods for querying the DB'''
# pylint: disable = redefined-outer-name

from inspect import isgeneratorfunction

import psycopg2.errors

from _test_db import PostgresTestDB, SqliteTestDB


verbose = False


def run_method(method, *args, **kwargs):
    '''Run method and consume values returned'''

    if isgeneratorfunction(method):
        _values = list(method(*args, **kwargs))
    else:
        _values = method(*args, **kwargs)

    if verbose:
        print('\nmethod =', method.__name__)
        print('statement =', args[0])
        print('positional args =', args[1:])
        print('named args =', kwargs)
        print(_values)


def test_query_methods(db):
    '''Test query methods for this DB.'''

    if verbose:
        print('\ndb_type =', db.db_type)

    # no params
    statement = 'SELECT * FROM test'
    for method in [db.query, db.fetch, db.fetchall, db.fetchone, db.execute]:
        run_method(method, statement)

    # positional params
    placeholder = db.placeholder()
    statement = 'SELECT * FROM test WHERE num > {ph} and num < {ph}'.format(
        ph=placeholder)

    values = [300, 700]

    for method in [db.query, db.fetch, db.fetchall, db.fetchone, db.execute]:
        run_method(method, statement, values)
        run_method(method, statement, *values)

    # named params
    placeholder1 = db.placeholder(keyword='_min')
    placeholder2 = db.placeholder(keyword='_max')
    statement = 'SELECT * FROM test WHERE num > {} and num < {}'.format(
        placeholder1, placeholder2)

    for method in [db.query, db.fetch, db.fetchall, db.fetchone, db.execute]:
        run_method(method, statement, _min=300, _max=700)
        run_method(method, statement, {'_min': 300, '_max': 700})


def test_execute_values(db):
    '''Test execute_values().'''
    # pylint: disable = no-member

    # execute_values() is only available for postgresql
    if db.db_type != 'postgres':
        return

    if verbose:
        print('\nmethod = execute_values')

    values = [1001, 1002]

    # make sure the values don't currently exist in the table
    delete_statement = 'DELETE FROM test WHERE num IN (%s, %s)'
    db.execute(delete_statement, values)

    # insert with execute_values()
    insert_statement = 'INSERT INTO test (num) VALUES %s'
    values_list = [[i] for i in values]

    db.execute_values(insert_statement, values_list)

    # check that the insertion was successful
    select_template = 'SELECT * FROM test WHERE num IN ({})'

    select_statement = select_template.format(', '.join(['%s']*len(values)))
    rows = db.fetchall(select_statement, values)

    assert(len(rows) == 2), 'Failed to insert values with execute_values()'

    if verbose:
        print('statement =', insert_statement)
        print('positional args =', values_list)
        print('rows inserted =', rows)

    # check that if we trigger an error, the transaction gets aborted and no new
    # values are inserted
    new_value = 1003
    values_list.append([new_value])

    try:
        db.execute_values(insert_statement, values_list)
    except psycopg2.errors.UniqueViolation:
        if verbose:
            print('successfully aborted transaction')
    else:
        raise AssertionError(
            'Unique contraint violation not raised by psycopg2')

    # make sure the new value was not inserted
    select_statement = select_template.format('%s')
    assert not db.fetchone(select_statement, new_value), (
        'New value inserted despite aborted transaction')

    # clean up inserted values
    db.execute(delete_statement, values)


if __name__ == '__main__':
    for db in [PostgresTestDB(), SqliteTestDB()]:
        try:
            test_query_methods(db)
            test_execute_values(db)
        except:
            # log DB type where the exception occurred
            print('Caught exception at {} DB'.format(db.db_type))
            raise
