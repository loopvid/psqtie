#! /usr/bin/env python3
'''Test user error checking.'''

from psqtie import DB, DBError, QueryFactory, WhereClause

from _test_db import PostgresTestDB, SqliteTestDB

verbose = False


def assert_error(function, error_class, error_message, *args, **kwargs):
    '''Test if a function raises an error when called with certain arguments.'''
    try:
        function(*args, **kwargs)
    except error_class:
        pass
    else:
        raise AssertionError(error_message)


def test_creation_error():
    '''Test errors while creating a new DB object.'''

    if verbose:
        print('creating db without db_path or db_name')

    assert_error(DB, DBError, 'Created DB object without path or name')


def test_query_factory_errors():
    '''Test query factory user errors.'''

    if verbose:
        print('testing query factory errors')

    assert_error(
        WhereClause, TypeError,
        'Created WhereClause with wrong type for conditions',
        conditions={'values': 1}
    )

    assert_error(
        QueryFactory.build_insert_query, DBError,
        'Built insert query without values',
        'test', {}
    )

    assert_error(
        QueryFactory.build_insert_query, DBError,
        'Built insert query with conflicting arguments',
        'test', {'value': 0},
        replace=True, upsert_on='value', on_conflict='DO NOTHING'
    )

    assert_error(
        QueryFactory.build_update_query, DBError,
        'Built update query without a where clause',
        'test', {'value': 0}, {}
    )

    assert_error(
        QueryFactory.build_delete_query, DBError,
        'Built delete query without a where clause',
        'test', {}
    )

    assert_error(
        QueryFactory.get_placeholder, ValueError,
        'Returned placeholder with unsafe chars',
        'postgres', keyword="; DROP TABLE test; "
    )


def test_common_errors(db):
    '''Test user errors that are common to both types.'''
    # pylint: disable = protected-access

    if verbose:
        print('testing errors that are common to both types')

    if verbose:
        print('execute empty schema')

    schema = db._db_schema
    db.set_schema(None)

    assert_error(
        db.execute_schema, DBError,
        'Executed empty schema'
    )

    db.set_schema(schema)


def test_postgres_errors(db):
    '''Test user errors that are specific to postgresql.'''
    # pylint: disable = protected-access

    if verbose:
        print('testing postgresql specific errors')


    if verbose:
        print('creating connection pool with invalid values')

    assert_error(
        db._create_connection_pool, ValueError,
        'Created connection pool without min_conn and max_conn'
    )

    db._min_conn = -1
    db._max_conn = -2

    assert_error(
        db._create_connection_pool, ValueError,
        'Created connection pool with invalid min_conn and max_conn'
    )

    db._min_conn = db._max_conn = None


    if verbose:
        print('executing without a statement')

    assert_error(
        db.fetchall, DBError,
        'Executed query without a statement'
    )


    if verbose:
        print('executing statement with both positional and named args')

    statement = 'SELECT * FROM test WHERE value = {} OR value = {}'.format(
        db.placeholder(), db.placeholder('value'))
    values1 = [0]
    values2 = {'value': 1}

    assert_error(
        db.fetchall, ValueError,
        'Accepted positional and named args at the same time',
        statement, *values1, **values2
    )


    if verbose:
        print('passing the wrong column to insert_or_ignore()')

    assert_error(
        db.insert_or_ignore, ValueError,
        'insert_or_ignore() accepted wrong column name',
        'test', {'value': 0}, conflicting_cols='values'
    )


    if verbose:
        print('testing query factory errors specific to postgresql')

    assert_error(
        db.insert_into, DBError,
        'Built insert query for postgresql using REPLACE',
        'test', {'num': 1}, replace=True
    )


def test_sqlite_errors(db):
    '''Test user errors that are specific to sqlite.'''
    # pylint: disable = protected-access

    if verbose:
        print('testing sqlite specific errors')

    assert not db._connect_kwargs, 'Connect kwargs generated for sqlite'

    if verbose:
        print('creating connection pool on sqlite')

    assert_error(
        db._create_connection_pool, NotImplementedError,
        'Created connection pool for sqlite'
    )

    if verbose:
        print('executing with invalid parameters')

    statement = 'SELECT * FROM test WHERE value = ?'
    values = [0]

    with db._get_connection() as connection:
        assert_error(
            db._execute, DBError,
            'Executed sqlite script with parameter substitution',
            connection, statement, values, script=True
        )

        assert_error(
            db.execute_values, DBError,
            'Used execute_values() on sqlite',
            statement, [values]
        )

        assert_error(
            db._execute, DBError,
            'Used execute_values() on sqlite',
            connection, statement, [values], use_execute_values=True
        )


    if verbose:
        print('testing query factory errors specific to sqlite')

    assert_error(
        db.insert_into, DBError,
        'Built insert query for postgresql using RETURNING',
        'test', {'num': 1}, returning='test_id'
    )


if __name__ == '__main__':

    test_creation_error()
    test_query_factory_errors()

    for _i, _class in enumerate([PostgresTestDB, SqliteTestDB]):
        try:
            _db = _class(auto_commit=False)

            if _i == 0:
                test_common_errors(_db)

            if _db.db_type == 'postgres':
                test_postgres_errors(_db)
            else:
                test_sqlite_errors(_db)

            _db.rollback()
        except:
            # log DB type where the exception occurred
            print('Caught exception at {}'.format(_class.__name__))
            raise
