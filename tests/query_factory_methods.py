#! /usr/bin/env python3
'''Test convenience methods that make use of QueryFactory.'''
# pylint: disable = redefined-outer-name

import random
from os.path import exists

from _test_db import PostgresTestDB, SqliteTestDB


verbose = False

random.seed()


def get_new_values(db):
    '''Generate two new values not already present in the test table.'''

    new_values = []
    i = 0 # try to catch infinite loops
    while len(new_values) < 2:
        i += 1
        assert i < 1000, ("Couldn't find new values to test methods; try "
                          "recreating the test DBs")

        # choose a random value
        candidate = random.randint(0, 1000)

        # check if it's already present
        try:
            if not db.select_from('test', {'num': candidate}):
                # candidate is not already present, so save it
                new_values.append(candidate)
        except:
            # log which function generated the exception
            print('Caught exception at DB.select_from()\n\n')
            raise

    if verbose:
        print('new values =', new_values)

    return new_values


def test_select_from(db):
    '''Test select_from().'''
    # select using all options available
    try:
        db.select_from(
            'test', {'num': 1, 'upserted': None},
            comparisons=[('num', '>', 0), ('num', '<', 1000)],
            columns=['test.id', 'test.num'],
            joins={'test_joins': ('num', 'id')},
            append_to_statement='LIMIT 1', one=False
        )
    except:
        # log which function generated the exception
        print('Caught exception at DB.select_from()\n\n')
        raise

    if verbose:
        print('tested select_from()')


def test_insert_upsert(db, new_value):
    '''Test DB.insert_into() by inserting one new value and upserting it. Test
    DB.insert_or_ignore() by attempting to insert the new value twice.'''

    returning = 'num' if db.db_features['returning'] else None

    try:
        # NOTE that if returning is not available, we return the cursor and
        # check its lastrowid if this is sqlite (since psycopg2 doesn't set
        # lastrowid)
        _r = db.insert_into('test', {'num': new_value}, returning=returning,
                            return_cursor=(not returning))
        if returning:
            assert _r, 'RETURNING failed to return anything on insert'
        else:
            assert _r, 'Failed to return cursor'
            if db.db_type == 'sqlite':
                assert _r.lastrowid, 'SQLite failed to set lastrowid'

        if db.db_features['replace']:
            # attempt to replace with the same number
            db.insert_into('test', {'num': new_value}, replace=True)

        if db.db_features['upsert']:
            # test upserting all values (in this case, replacing num with
            # itself)
            _r = db.insert_into('test', {'num': new_value}, upsert_on='num',
                                returning=returning)
            assert _r or not returning, (
                'RETURNING failed to return anything on upsert')

            # test upserting specific columns
            _r = db.insert_into(
                'test', {'num': new_value}, upsert_on='num',
                upsert_values={'upserted': 1}, returning=returning)
            assert _r or not returning, (
                'RETURNING failed to return anything on upsert')

            # test do nothing
            _r = db.insert_into(
                'test', {'num': new_value}, on_conflict='DO NOTHING',
                returning=returning)
            assert not returning or _r is None, (
                'RETURNING + DO NOTHING returned something')
    except:
        # log which function generated the exception
        print('Caught exception at DB.insert_into()\n\n')
        raise

    row = db.select_from('test', {'num': new_value})
    assert row, 'Error inserting value'
    assert row['upserted'] or not db.db_features['upsert'], 'Error upserting'

    # delete row and test insert_or_ignore()
    db.delete_from('test', {'num': new_value})

    try:
        assert db.insert_or_ignore('test', {'num': new_value}, 'num'), (
            'insert_or_ignore() failed to insert for the first time')

        assert not db.insert_or_ignore('test', {'num': new_value}, 'num'), (
            'insert_or_ignore() failed to ignore an existing value')
    except:
        # log which function generated the exception
        print('Caught exception at DB.insert_or_ignore()\n\n')
        raise

    if verbose:
        print('inserted new value', new_value)


def test_update(db, new_values):
    '''Test DB.update() by updating the previously inserted value.'''
    try:
        # note that the comparisons have no effect on the query
        db.update('test', {'num': new_values[1]}, {'num': new_values[0]},
                  comparisons=[('num', '>', 0), ('num', '<', 1000)])
    except:
        # log which function generated the exception
        print('Caught exception at DB.update()\n\n')
        raise

    assert db.select_from('test', {'num': new_values[1]}), 'Error updating row'

    if verbose:
        print('updated value to ', new_values[1])


def test_delete(db, new_values):
    '''Test DB.delete_from() by deleting the previously inserted row.'''
    try:
        # note that the comparisons have no effect on the query
        db.delete_from('test', {'num': new_values[1]},
                       comparisons=[('num', '>', 0), ('num', '<', 1000)])
    except:
        # log which function generated the exception
        print('Caught exception at DB.delete_from()\n\n')
        raise

    statement = 'SELECT num FROM test WHERE num={ph} OR num={ph}'.format(
        ph=db.placeholder())
    assert not db.fetchone(statement, new_values), 'Error deleting row'

    if verbose:
        print('deleted new row')


def test_methods(db):
    '''Test all methods for this DB, by picking two random values which are not
    already present in the test table and then inserting, updating and deleting
    the new row.'''

    if verbose:
        print('\ndb_type =', db.db_type)
        print('\ndb_features =', db.db_features)

    new_values = get_new_values(db)
    test_select_from(db)
    test_insert_upsert(db, new_values[0])
    test_update(db, new_values)
    test_delete(db, new_values)


if __name__ == '__main__':
    # silently test printing debug information, if /dev/null is available
    devnull = open('/dev/null', 'w') if exists('/dev/null') else None

    for _class in [PostgresTestDB, SqliteTestDB]:
        try:
            db = _class(auto_commit=False, debug_file=devnull)
            test_methods(db)
            db.rollback()
        except:
            # log DB type where the exception occurred
            print('Caught exception at {} DB\n\n'.format(db.db_type))
            raise

    if devnull:
        devnull.close()
