#! /usr/bin/env python3
'''Check that connections are being properly closed in auto commit mode.

This test is more important for postgres because it has a hard limit on the
number of concurrent connections (the default is 100), while sqlite's actual
limit seems to be whatever the system allows it to open.
'''
# pylint: disable = redefined-outer-name

import sys

from _test_db import PostgresTestDB, SqliteTestDB


verbose = False


def test_connection_closing(db_class):
    '''Check that connections are being properly closed by making enough queries
    that if connections were being left over, we would reach the connection
    limit. Test connection closing both when performing many queries with a
    single object and when creating one object per query.'''

    statement = 'SELECT * FROM test'

    # default max_connections for postgres is 100
    total = 200

    # test repetitively querying with a single object
    db = db_class()
    for i in range(total):
        # make sure the connection still gets closed despite returning a cursor
        cursor = db.execute(statement)
        assert cursor, 'execute() returned nothing'

        if verbose:
            print('\r{}/{}'.format(i+1, total), end='', file=sys.stderr)
    if verbose:
        print(file=sys.stderr)

    # test instantiating new objects
    for i in range(total):
        # make sure the connection still gets closed despite returning a cursor
        cursor = db_class().execute(statement)
        assert cursor, 'execute() returned nothing'

        if verbose:
            print('\r{}/{}'.format(i+1, total), end='', file=sys.stderr)
    if verbose:
        print(file=sys.stderr)


if __name__ == '__main__':
    # PooledPostgresTestDB will be tested in pool_max_connections.py
    for db_class in [PostgresTestDB, SqliteTestDB]:
        test_connection_closing(db_class)
