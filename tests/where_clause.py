#! /usr/bin/env python3
'''Test WhereClause.'''
# pylint: disable = redefined-outer-name

from psqtie import WhereClause

from _test_db import PostgresTestDB, SqliteTestDB


verbose = False

def test_where_clause(db):
    '''Test WhereClause'''
    test_num = 500

    if not db.select_from('test', {'num': test_num}):
        db.insert_into('test', {'num': test_num})

    ph = db.placeholder()

    # using named args and passing a lists
    where_clause = WhereClause(
        conditions=['num IS NOT NULL', 'num > {}'.format(ph)],
        values=(test_num-1,)
    )

    # adding WhereClause()s, not naming args and passing values directly
    where_clause += WhereClause('num < {}'.format(ph), test_num+1)

    # passing tuples
    where_clause += WhereClause(('num != {}'.format(ph),),
                                (test_num*2,))

    # accessing conditions and values directly
    where_clause.conditions.append('num = {}'.format(ph))
    where_clause.values.append(test_num)

    # converting to str
    statement = 'SELECT * FROM test {where}'.format(where=where_clause)
    values = where_clause.values

    if verbose:
        print(statement, values)

    assert db.fetchone(statement, values), 'Error selecting value'


if __name__ == '__main__':
    for _class in [PostgresTestDB, SqliteTestDB]:
        try:
            db = _class(auto_commit=False)
            test_where_clause(db)
            db.rollback()
        except:
            # log DB type where the exception occurred
            print('Caught exception at {}'.format(_class.__name__))
            raise
