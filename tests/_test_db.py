#! /usr/bin/env python3
'''Test DBs for running tests on'''

from os.path import dirname, join

from psqtie import DB


class SqliteTestDB(DB):
    '''Test DB for sqlite3'''
    _db_path = join(dirname(__file__), 'test.db')

class PostgresTestDB(DB):
    '''Test DB for psycopg2'''
    _db_name = 'test_db'

class PooledPostgresTestDB(DB):
    '''Test DB for psycopg2 with connection pooling enabled'''
    _db_name = 'test_db'
    # low values so we can better test our handling of the pool getting
    # exhausted (since if max_conns is too high, python might not be able to
    # create new threads faster than the pool returns queries, so we never hit
    # the max)
    _min_conn = 2
    _max_conn = 4
