#! /usr/bin/env python3
'''Check that connections are not being shared across threads in multi-threaded
applications (only in auto commit mode, otherwise the caller is the one
responsible for enforcing these conditions on the shared connection).
'''
# pylint: disable = redefined-outer-name, global-statement

import sys
import threading

from _test_db import PostgresTestDB, SqliteTestDB


verbose = False
threads_done = 0
total = 20


def run_a_query(db):
    '''Run a query using a DB object.'''
    global threads_done

    statement = 'SELECT COUNT(*) FROM test'
    db.fetchone(statement)

    threads_done += 1

    if verbose:
        print('\r{}/{}  '.format(threads_done, total), end='',
              file=sys.stderr)

def test_threading(db_class):
    '''Check that connections are not being shared across threads in
    multi-threaded applications (only in auto commit mode, otherwise the caller
    is the one responsible for enforcing these conditions on the shared
    connection).'''
    # pylint: disable = unused-variable

    global threads_done

    threads_done = 0

    db = db_class()

    for i in range(total):
        t = threading.Thread(target=run_a_query, args=[db], daemon=True)
        t.start()

        if verbose:
            print('\r{}/{}  '.format(threads_done, total), end='',
                  file=sys.stderr)

    # wait for all threads to finish
    while threads_done < total:
        for t in threading.enumerate():
            if t == threading.current_thread():
                continue
            else:
                t.join()

    if verbose:
        print(file=sys.stderr)


if __name__ == '__main__':
    # PooledPostgresTestDB will be tested in pool_max_connections.py
    for db_class in [PostgresTestDB, SqliteTestDB]:
        test_threading(db_class)
