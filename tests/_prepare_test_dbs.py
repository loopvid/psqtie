#! /usr/bin/env python3
'''Prepare test DBs for running tests on.'''
# pylint: disable = redefined-outer-name

import random
from sqlite3 import OperationalError

from psycopg2 import ProgrammingError

from _test_db import SqliteTestDB, PostgresTestDB


random.seed()

def create_and_populate_table(db):
    '''Create test table and populate it with random values.'''
    # pylint: disable = unused-variable

    # try cleaning up an old table if it exists
    tables = ['test', 'test_joins']
    for table in tables:
        try:
            db.execute('DROP TABLE {}'.format(table))
            db.commit()
        except (OperationalError, ProgrammingError):
            db.rollback()

    if db.db_type == 'postgres':
        db.set_schema('''
            CREATE TABLE test (
                id SERIAL PRIMARY KEY NOT NULL,
                num INTEGER UNIQUE NOT NULL,
                upserted INTEGER DEFAULT 0
                );
            CREATE TABLE test_joins (id INTEGER PRIMARY KEY);
            ''')
        statement = 'INSERT INTO test (num) VALUES (%s)'
    else:
        db.set_schema('''
            CREATE TABLE test (
                id INTEGER PRIMARY KEY NOT NULL,
                num INTEGER UNIQUE NOT NULL,
                upserted INTEGER DEFAULT 0
                );
            CREATE TABLE test_joins (id INTEGER PRIMARY KEY);
            ''')
        statement = 'INSERT INTO test (num) VALUES (?)'

    # create table
    db.execute_schema()

    # insert 10 random ints (without repetition)
    values = list(range(1000))
    random.shuffle(values)
    for i in range(10):
        num = values.pop()

        db.execute(statement, num)


if __name__ == '__main__':
    for _class in [PostgresTestDB, SqliteTestDB]:
        db = _class(auto_commit=False)
        create_and_populate_table(db)
        db.commit()
