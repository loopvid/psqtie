#! /usr/bin/env python3
'''Test auto commit mode.'''
# pylint: disable = redefined-outer-name

import random

from _test_db import PostgresTestDB, PooledPostgresTestDB, SqliteTestDB


verbose = False

random.seed()

def get_new_value(_class):
    '''Generate new value not already present in the test table.'''
    db = _class()

    i = 0 # try to catch infinite loops
    while True:
        i += 1
        assert i < 1000, ("Couldn't find new values to test methods; try "
                          "recreating the test DBs")

        # choose a random value
        candidate = random.randint(0, 1000)

        # check if it's already present
        if not db.select_from('test', {'num': candidate}):
            # candidate is not already present, so return it
            if verbose:
                print('new value =', candidate)

            return candidate

def insert_with_auto_commit(_class, new_value):
    '''Instantiate DB with auto commit enabled and insert the new value.'''
    db = _class(auto_commit=True)
    db.insert_into('test', {'num': new_value})

def insert_without_auto_commit(_class, new_value, commit_manually=False):
    '''Instantiate DB with auto commit disabled and insert the new value.'''
    with _class(auto_commit=False) as db:
        db.insert_into('test', {'num': new_value})

        if commit_manually:
            db.commit()

def select_value(_class, value):
    '''Instantiate DB and select row containing the value.'''
    db = _class()
    return db.select_from('test', {'num': value})

def delete_value(_class, value):
    '''Instantiate DB and delete row containing the value.'''
    db = _class(auto_commit=True)
    db.delete_from('test', {'num': value})

def test_auto_commit(_class):
    '''Test auto commit mode.'''

    new_value = get_new_value(_class)

    # try inserting the new value with auto_commit and check that it got
    # committed
    if verbose:
        print('inserting and seleting value with auto commit enabled')

    insert_with_auto_commit(_class, new_value)
    assert select_value(_class, new_value), ('auto commit mode failed to commit'
                                             'changes')

    if verbose:
        print('deleting value with auto commit enabled')

    delete_value(_class, new_value)
    assert not select_value(_class, new_value), ('auto commit mode failed to '
                                                 'commit changes')

    # try inserting new value without auto_commit and check that it did not get
    # committed
    if verbose:
        print('inserting and selecting value with auto commit disabled')

    insert_without_auto_commit(_class, new_value, commit_manually=False)
    try:
        assert not select_value(_class, new_value), ('changes got committed '
                                                     'without auto commit')
    except:
        # try to revert the change that got wrongfully committed
        delete_value(_class, new_value)
        raise

    # try inserting the new value and committing manually
    if verbose:
        print('inserting and selecting value with auto commit disabled but '
              'manually committing the transaction')

    insert_without_auto_commit(_class, new_value, commit_manually=True)
    assert select_value(_class, new_value), ('manual commit failed')

    if verbose:
        print('deleting value manually committed')

    delete_value(_class, new_value)
    assert not select_value(_class, new_value), ('auto commit mode failed to '
                                                 'commit changes')


if __name__ == '__main__':
    # note that PostgresTestDB and PooledPostgresTestDB will behave exactly the
    # same when auto_commit=False, but we still want to test them separately
    # when auto_commit=True
    for _class in [PostgresTestDB, PooledPostgresTestDB, SqliteTestDB]:
        try:
            test_auto_commit(_class)
        except:
            # log DB type where the exception occurred
            print('Caught exception at {}'.format(_class.__name__))
            raise
