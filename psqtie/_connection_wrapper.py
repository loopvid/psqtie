#! /usr/bin/env python3
'''Wrapper for auto committing and closing transient connections'''

class ConnectionWrapper:
    '''Wrapper for handling connections according to auto commit mode'''
    # pylint: disable = too-few-public-methods

    def __init__(self, connection, auto_commit, pool=None, key=None):
        self._connection = connection
        self._auto_commit = auto_commit
        self._pool = pool
        self._key = key

    def cursor(self):
        '''Return a cursor.'''
        return self._connection.cursor()

    def __enter__(self):
        return self

    def __exit__(self, _type, value, trace):
        '''Commit and close connection if in auto commit mode, otherwise do
        nothing.'''

        if self._auto_commit:
            self._connection.commit()
            if self._pool:
                # when pooling, put away (i.e. free up) connection instead of
                # closing it
                self._pool.putconn(self._connection, key=self._key)
            else:
                self._connection.close()
