#! /usr/bin/env python3
'''Functions for building SQL queries'''

import re

from ._exceptions import DBError


class WhereClause:
    '''Conditions and values for manually building a where clause

    Example usage:

    clause = WhereClause(conditions=['table1.col1 = %s'], values=['value1'])
    clause += WhereClause('table1.col2 IS NULL')

    clause.conditions.append('table1.col3 LIKE %s')
    clause.values.append('%value3%')

    statement = 'SELECT * FROM table1 {where}'.format(where=clause)
    cursor = connection.execute(statement, clause.values)
    '''
    # pylint: disable = too-few-public-methods

    def __init__(self, conditions=None, values=None):
        self.conditions = []
        self.values = []

        if conditions:
            if isinstance(conditions, (list, tuple)):
                self.conditions += list(conditions)
            elif isinstance(conditions, str):
                self.conditions.append(conditions)
            else:
                raise TypeError(
                    'Wrong type for conditions (expected list or str): '
                    '{}'.format(type(conditions)))

        if values:
            if isinstance(values, (list, tuple)):
                self.values += list(values)
            else:
                self.values.append(values)

    def __str__(self):
        '''Assembled where clause containing all conditions'''
        return ' WHERE ' + ' AND '.join(self.conditions)

    def __add__(self, other):
        '''Add two WhereClause() objects by concatenating their conditions and
        values'''
        conditions = self.conditions + other.conditions
        values = self.values + other.values
        return type(self)(conditions, values)


class QueryFactory:
    '''Functions for building SQL queries'''

    # unsafe chars for named params
    _unsafe_chars = re.compile('[^0-9A-Za-z_]')

    @classmethod
    def build_select_query(cls, table_name, equalities, comparisons=None,
                           columns='*', joins=None,
                           append_to_statement='', db_type='postgres'):
        '''Build a SELECT query.

        WARNING: table_name, append_to_statement and each key and operator must
        always be hardcoded since they are subject to injection attacks.

        columns can be a string containing a list of columns to be selected, or
        a proper list of strings, e.g.
            columns='name, account.name AS username'
            columns=['id', 'count(*)']

        See _build_where_clause() for a description of how to use equalities and
        comparisons.

        See _build_join_clause() for a description of how to use joins.

        You can use append_to_statement to order or limit the results.
        '''
        # make columns into a string
        if isinstance(columns, (list, tuple, set)):
            columns = ', '.join(columns)

        where_clause, values = cls._build_where_clause(
            equalities, comparisons, db_type)

        join_clause = cls._build_join_clause(table_name, joins)

        statement = 'SELECT {} FROM {} {} {} {}'.format(
            columns, table_name, join_clause, where_clause, append_to_statement)

        return statement, values

    @classmethod
    def build_insert_query(cls, table_name, values_dict, replace=False,
                           upsert_on=None, upsert_values=None, on_conflict=None,
                           returning=None, db_type='postgres'):
        '''Build an INSERT query.

        Note that replace, upsert_on and on_conflict are mutually exclusive.

        WARNING: table_name, usert_on, on_conflict, returning and each key in
        the values dicts must always be hardcoded since they are subject to
        injection attacks.
        '''

        if not values_dict:
            raise DBError('Missing values_dict')

        if len(list(filter(None, (replace, upsert_on, on_conflict)))) > 1:
            raise DBError(
                'replace, upsert_on and on_conflict are mutually exclusive')

        # build cols and value placeholders clauses
        cols = []
        value_placeholders = []
        values = []
        for key, value in values_dict.items():
            cols.append(key)
            value_placeholders.append(cls.get_placeholder(db_type))
            values.append(value)
        cols_clause = ','.join(cols)
        placeholders_clause = ','.join(value_placeholders)

        replace_clause = ' OR REPLACE' if replace else ''

        statement = 'INSERT{} INTO {} ({}) VALUES ({})'.format(
            replace_clause, table_name, cols_clause, placeholders_clause)

        if on_conflict:
            statement += ' ON CONFLICT {}'.format(on_conflict)
        elif upsert_on:
            # if no upsert values dict was provided, update all values
            if not upsert_values:
                upsert_values = dict(values_dict)

            set_clause, new_values = cls._build_set_clause(
                upsert_values, db_type)

            statement += ' ON CONFLICT({}) DO UPDATE SET {}'.format(
                upsert_on, set_clause)
            values += new_values

        if returning:
            statement += ' RETURNING {}'.format(returning)

        return statement, values

    @classmethod
    def build_update_query(cls, table_name, values_dict, equalities,
                           comparisons=None, db_type='postgres'):
        '''Build an UPDATE query.

        See _build_where_clause() for a description of how to use equalities and
        comparisons.
        '''
        # build values clause
        set_clause, values = cls._build_set_clause(values_dict, db_type)

        # build where clause
        where_clause, where_values = cls._build_where_clause(
            equalities, comparisons, db_type)
        values += where_values

        # refuse to run without a where clause
        if not where_clause:
            raise DBError(
                'Attempt to update table without a WHERE clause:'
                '\ntable={}, values={}, equalities={}, comparisons={}'.format(
                    table_name, values_dict, equalities, comparisons))

        statement = 'UPDATE {} SET {} {}'.format(table_name, set_clause, where_clause)

        return statement, values

    @classmethod
    def build_delete_query(cls, table_name, equalities, comparisons=None,
                           db_type='postgres'):
        '''Build a DELETE query.

        See _build_where_clause() for a description of how to use equalities and
        comparisons.
        '''
        # build where clause
        where_clause, values = cls._build_where_clause(
            equalities, comparisons, db_type)

        # refuse to run without a where clause
        if not where_clause:
            raise DBError(
                'Attempt to delete from table without a WHERE clause:'
                '\ntable={}, equalities={}, comparisons={}'.format(
                    table_name, equalities, comparisons))

        statement = 'DELETE FROM {} {}'.format(table_name, where_clause)

        return statement, values

    @classmethod
    def _build_where_clause(cls, equalities, comparisons, db_type):
        '''Builds WHERE clause consisting of the specified comparisons and list
        of values for it.

        For equality comparisons, you should use the equalities dict in the form
            {col: value, ...}
        e.g.
            {'age': 18}

        For other operations, you must use the comparisons list, in the form
            [(col, operator, value), ...]
        e.g.
            [('age', '>', 18)]
        '''

        placeholder = cls.get_placeholder(db_type)
        values = []
        comparison_strings = []
        if equalities:
            for key, value in equalities.items():
                if value is not None:
                    comparison_strings.append('{}={}'.format(key, placeholder))
                    values.append(value)
                else:
                    comparison_strings.append('{} IS NULL'.format(key))

        if comparisons:
            for _tuple in comparisons:
                key, operator, value = _tuple
                comparison_strings.append('{} {} {}'.format(
                    key, operator, placeholder))
                values.append(value)

        where_clause = ''
        if comparison_strings:
            where_clause = 'WHERE ' + ' AND '.join(comparison_strings)

        return where_clause, values

    @staticmethod
    def _build_join_clause(table, joins):
        '''Builds JOIN clause.

        joins should be a dict in the form
            {table_name: (column_in_one_table, column_in_other_table), ...}

        If the column names in the column pair do not contain a table name, the
        original table and the table being joined will be prepended to them
        automatically. For example:

            _build_join_clause('table1', {
                'table2': ('column1a', 'column2'),
                'table3': ('column1b', 'column3a'),
                'table4': ('table3.column3b', 'table4.column4')})

        will produce

            JOIN
                table2 ON table1.column1a=table2.column2,
                table3 ON table1.column1b=table3.column3a,
                table4 ON table3.column3b=table4.column4
        '''

        join_clause = ''
        if joins:
            subclauses = []
            # build subclause for each table being joined
            for other_table, column_pair in joins.items():
                column1, column2 = column_pair

                # prepend table names if necessary (assuming the first column
                # belongs to the orignal table and the seconds one to the table
                # being joined)
                if not '.' in column1:
                    column1 = table+'.'+column1
                if not '.' in column2:
                    column2 = other_table+'.'+column2

                # make subclause
                subclause = '{} ON {}={}'.format(other_table, column1, column2)
                subclauses.append(subclause)

            join_clause += ' JOIN ' + ', '.join(subclauses)

        return join_clause

    @classmethod
    def _build_set_clause(cls, values_dict, db_type):
        '''Builds set clause and list containing the values for it.'''
        assert values_dict, 'Missing values_dict'

        value_placeholders = []
        values = []
        for key, value in values_dict.items():
            value_placeholders.append('{}={}'.format(
                key, cls.get_placeholder(db_type)))
            values.append(value)
        placeholders_clause = ','.join(value_placeholders)

        return placeholders_clause, values

    @classmethod
    def get_placeholder(cls, db_type, keyword=None):
        '''Return correct placeholder to use for a positional or named
        parameter, according to the DB type.

        WARNING: if using named params, make sure to hardcode the keyword since
        it is subject to injection attacks. As a safeguard, this method will
        reject keywords containing characters outside of [0-9A-Za-z_].
        '''

        # named param
        if keyword:
            # check for unsafe chars in keyword
            if cls._unsafe_chars.search(keyword):
                raise ValueError('Illegal chars in keyword {}'.format(keyword))

            if db_type == 'postgres':
                return '%({})s'.format(keyword)
            else:
                return ':{}'.format(keyword)

        # positional param
        else:
            if db_type == 'postgres':
                return '%s'
            else:
                return '?'
