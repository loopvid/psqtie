#! /usr/bin/env python3
'''Convenience methods for accessing the DB'''

import time

try:
    # sqlite3 is not available for all OSs
    import sqlite3
except ImportError:
    pass

try:
    import psycopg2
    import psycopg2.errors
    from psycopg2.pool import ThreadedConnectionPool, PoolError
    from psycopg2.extras import (NamedTupleCursor,
                                 execute_values as _execute_values)
except ImportError:
    pass

from ._exceptions import DBError
from ._query_factory import QueryFactory as QF
from ._connection_wrapper import ConnectionWrapper


class DB:
    '''Convenience methods for accessing the DB'''
    # pylint: disable = too-many-instance-attributes

    # child classes should set these, if using postgres
    _db_name = None
    _user = None
    _password = None
    _host = None
    _port = None
    # or this, if using sqlite
    _db_path = None

    # connection pooling
    _min_conn = None
    _max_conn = None
    _pool = None

    _db_schema = None

    _debug_file = False

    def __init__(self,
                 db_name=None, user=None, password=None, host=None, port=None,
                 db_path=None,
                 min_conn=None, max_conn=None,
                 auto_commit=True,
                 schema=None,
                 debug_file=None):
        '''Create new object for interfacing the DB.

        Note that if you're using postgres, you shouldn't pass db_path, while if
        you're using sqlite, you shouldn't pass db_name, user or password.

        Connection pooling is only available for postgres. To use it, both
        min_conn and max_conn must be set. Note that pooling will be ignored
        when auto_commit is False (see below).

        If auto_commit is True, a new transient connection will be created (or
        one of the pool's connections will be selected) and automatically
        committed and closed for every operation. If it is False, a single
        connection will be created and used for all operations performed by this
        object and it will need to be manually committed and closed. Note that
        in this case, it's advisable not to share the object across different
        threads.

        It's also noteworthy that sqlite3 does not support executing certain
        commands within transactions (notably CREATE TABLE, DROP TABLE) and will
        auto commit even with auto_commit=False. See
        https://docs.python.org/3/library/sqlite3.html#controlling-transactions

        Set debug_file=sys.stderr or any file object in order to print debug
        information to that file.
        '''

        self._db_name = db_name if db_name is not None else self._db_name
        self._user = user if user is not None else self._user
        self._password = password if password is not None else self._password
        self._host = host if host is not None else self._host
        self._port = port if port is not None else self._port

        self._db_path = db_path if db_path is not None else self._db_path

        if not (self._db_name or self._db_path):
            raise DBError('Impossible to connect: no DB name or path provided')

        self._min_conn = min_conn if min_conn is not None else self._min_conn
        self._max_conn = max_conn if max_conn is not None else self._max_conn

        self.auto_commit = auto_commit
        self._db_schema = schema if schema is not None else self._db_schema
        self._debug_file = (debug_file if debug_file is not None
                            else self._debug_file)

        # discover and save DB information, including the type (postgres or
        # sqlite), so we know how to make new connections
        self._set_db_info()

        # create a shared connection when applicable
        if not self.auto_commit:
            self._shared_connection = self.connect()
        # create a connection pool when applicable
        elif self.auto_commit and (self._min_conn or self._max_conn):
            self._create_connection_pool()

        # try to connect once in order to raise an error if something is wrong
        with self._get_connection():
            pass

    def _set_db_info(self):
        '''Discover DB information, namely its type (postgres or sqlite),
        version and supported features.'''

        # determine DB type

        # NOTE that if both postgres and sqlite DB parameters are present,
        # postgres will take precedence
        self.db_type = 'postgres' if self._db_name else 'sqlite'

        # determine version using a transient connection (since we can't use
        # self.fetch() yet)
        _statement = ('SHOW server_version' if self.db_type == 'postgres'
                      else 'SELECT sqlite_version()')

        self.db_version = None
        self.db_version_tuple = None

        try:
            with ConnectionWrapper(self.connect(), True) as _connection:
                _cursor = self._execute(_connection, _statement, [])
                self.db_version = _cursor.fetchone()[0]

            # generate version tuple ('1.2.3' -> [1, 2, 3])
            self.db_version_tuple = [int(i) for i in self.db_version.split('.')]
        except (ValueError, IndexError, TypeError):
            pass

        # determine supported features
        _has_upsert = False
        _v = self.db_version_tuple
        if _v and len(_v) >= 2:
            # postgres supports upsert since version 9.5
            _postgres_upsert = self.db_type == 'postgres' and (
                _v[0] > 9 or (_v[0] == 9 and _v[1] >= 5))

            # sqlite supports upsert since version 3.24
            _sqlite_upsert = self.db_type == 'sqlite' and (
                _v[0] > 3 or (_v[0] == 3 and _v[1] >= 24))

            _has_upsert = _postgres_upsert or _sqlite_upsert

        self.db_features = {
            'returning': self.db_type == 'postgres',
            'replace': self.db_type == 'sqlite',
            'upsert': _has_upsert,
        }

    @property
    def _connect_kwargs(self):
        '''Generate arguments to be passed to psycopg2.connect().'''

        if self.db_type != 'postgres':
            # only postgres needs kwargs to connect
            return {}

        return {
            'dbname': self._db_name,
            'user': self._user,
            'password': self._password,
            'host': self._host,
            'port': self._port,
            'cursor_factory': NamedTupleCursor,
        }

    def connect(self, key=None, max_tries=100, retry_time=0.1):
        '''Make and return a new connection to the DB.

        WARNING: The caller is responsible for committing any changes and
        closing the connection.

        key identifies a particular connection in the connection pool. max_tries
        and retry_time determine how many times and for how long we will block
        while trying to obtain a free connection from the pool, when all of its
        connections are busy (if max_tries is None, we will retry indefinitely).
        All of these values will be ignored if pooling is not enabled.

        Example usage:

        with contextlib.closing(DB().connect()) as connection:
            # perform several operations
            connection.commit()
        '''

        if self._pool:
            # attempt to get a connection from the pool
            attempts = 0
            while max_tries is None or attempts < max_tries:
                try:
                    connection = self._pool.getconn(key=key)
                    break
                except PoolError as e:
                    if 'pool exhausted' in str(e):
                        attempts += 1
                        time.sleep(retry_time)
                        continue
                    raise

            if connection:
                return connection
            raise DBError('Timed out while trying to obtain a free '
                          'connection from the connection pool.')
        elif self.db_type == 'postgres':
            # connect to postgres
            try:
                return psycopg2.connect(**self._connect_kwargs)
            except NameError as e:
                # if psycopg2 is not available, raise a more explicit error
                # to inform the user
                if 'psycopg2' in str(e) or 'NamedTupleCursor' in str(e):
                    raise DBError('Please install psycopg2 in order to create '
                                  'postgres connections.')
                raise
        else:
            # connect to sqlite DB
            try:
                connection = sqlite3.connect(self._db_path)
            except NameError as e:
                # if sqlite3 is not available, raise a more explicit error
                # to inform the user
                if 'sqlite3' in str(e):
                    raise DBError('Please install sqlite3 in order to open '
                                  'sqlite3 databases.')
                raise

            connection.row_factory = sqlite3.Row
            return connection

    def _get_connection(self):
        '''Get new or shared connection wrapped in ConnectionWrapper according
        to auto commit mode.'''

        connection = (self.connect() if self.auto_commit
                      else self._shared_connection)

        return ConnectionWrapper(connection, self.auto_commit,
                                 pool=self._pool)

    def _create_connection_pool(self):
        '''Create a new connection pool.'''
        if self.db_type != 'postgres':
            raise NotImplementedError('Pooling is only implemented for '
                                      'postgresql')

        # check that min/max number of connections make sense
        if not (self._min_conn and self._max_conn):
            raise ValueError('To enable pooling, both min_conn and '
                             'max_conn must be set')
        if not 0 < self._min_conn <= self._max_conn:
            raise ValueError('min_conn cannot be negative or greater than '
                             'max_conn')

        # create pool
        try:
            self._pool = ThreadedConnectionPool(
                self._min_conn, self._max_conn, **self._connect_kwargs)
        except NameError as e:
            # if psycopg2 is not available, raise a more explicit error
            # to inform the user
            if ('psycopg2' in str(e) or 'ThreadedConnectionPool' in str(e)
                    or 'NamedTupleCursor' in str(e)):
                raise DBError('Please install psycopg2 in order to create '
                              'a postgres connection pool.')
            raise

    # methods for handling the shared connection when not in auto commit mode

    def close(self):
        '''Close shared connection if not in auto commit mode, otherwise do
        nothing.'''
        if not self.auto_commit:
            self._shared_connection.close()

    def commit(self):
        '''Commit shared connection if not in auto commit mode, otherwise do
        nothing.'''
        if not self.auto_commit:
            self._shared_connection.commit()

    def rollback(self):
        '''Rollback shared connection if not in auto commit mode, otherwise do
        nothing.'''
        if not self.auto_commit:
            self._shared_connection.rollback()

    def __enter__(self):
        return self

    def __exit__(self, _type, value, trace):
        self.close()

    # convenience methods for accessing the DB

    def placeholder(self, keyword=None):
        '''Return correct placeholder to use for a positional or named
        parameter, according to the DB type.

        WARNING: if using named params, make sure to hardcode the keyword since
        it is subject to injection attacks. As a safeguard, this method will
        reject keywords containing characters outside of [0-9A-Za-z_].
        '''
        return QF.get_placeholder(self.db_type, keyword=keyword)

    def _execute(self, connection, statement, values, script=False,
                 use_execute_values=False):
        '''Execute query using connection and return cursor.'''
        cursor = connection.cursor()
        if script and self.db_type == 'sqlite':
            if values:
                raise DBError('sqlite3.Cursor.executescript() does not support '
                              'parameter substitution')
            cursor.executescript(statement)
        elif use_execute_values:
            if self.db_type == 'sqlite':
                raise DBError('SQLite does not support execute_values()')
            _execute_values(cursor, statement, values)
        else:
            cursor.execute(statement, values)
        return cursor

    def query(self, *args, **kwargs):
        '''Generator for executing a query using the underlying lib.

        WARNING: this method may auto commit any changes made to the DB.

        This method accepts parameters to be substituted into the SQL statement
        both as a tuple or dict or as regular positional or named arguments.

        For example, these are all equivalent:

            query('SELECT * FROM user WHERE name = %s AND age > %s', ['John', 20])
            query('SELECT * FROM user WHERE name = %s AND age > %s', 'John', 20)

            query('SELECT * FROM user WHERE name = %(name)s AND age > %(age)s',
                  {'name': 'John', 'age': 20})
            query('SELECT * FROM user WHERE name = %(name)s AND age > %(age)s',
                   name='John', age=20)
        '''

        # separate statement and prepare values tuple or dict
        statement, values = self.split_statement_values(args, kwargs)

        with self._get_connection() as connection:
            for row in self._execute(connection, statement, values):
                yield row

    def fetch(self, *args, **kwargs):
        '''Generator that yields dicts for all matching rows.

        See query() for how to properly use arguments.
        '''

        for row in self.query(*args, **kwargs):
            yield self.row2dict(row)

    def fetchall(self, *args, **kwargs):
        '''Return list of dicts for all matching rows.

        See query() for how to properly use arguments.
        '''
        return list(self.fetch(*args, **kwargs))

    def fetchone(self, *args, **kwargs):
        '''Return a dict for the first matching row.

        See query() for how to properly use arguments.
        '''

        # separate statement and prepare values tuple or dict
        statement, values = self.split_statement_values(args, kwargs)

        with self._get_connection() as connection:
            cursor = self._execute(connection, statement, values)

            return self.row2dict(cursor.fetchone())

    def execute(self, *args, **kwargs):
        '''Execute query and return cursor.

        See query() for how to properly use arguments.
        '''
        # separate statement and prepare values tuple or dict
        statement, values = self.split_statement_values(args, kwargs)

        with self._get_connection() as connection:
            return self._execute(connection, statement, values)

    def execute_values(self, statement, values_lists):
        '''Execute query for each list of values, using psycopg2's
        execute_values().

        Note that the statement should contain a single '%s' placeholder where
        the values are supposed to be substituted, and that values_lists must be
        a list containing lists of values, e.g.
        [[1, 2, 3], [4, 5, 6]]
        will execute the statement for [1, 2, 3] and then for [4, 5, 6].
        '''

        if self.db_type == 'sqlite':
            raise DBError('SQLite does not support execute_values()')

        with self._get_connection() as connection:
            return self._execute(connection, statement, values_lists,
                                 use_execute_values=True)

    def executescript(self, script):
        '''Execute script. A script is a string containing one or several SQL
        statements separated by a semi-colon.

        Note that sqlite3 does not support parameter substitution for
        executescript(), which is why this method accepts no other arguments
        besides the script itself.

        Also note that for postgres, this method is merely an alias to
        execute(). In fact, all query methods (execute(), fetch(), etc) are able
        to handle scripts without a problem.
        '''
        with self._get_connection() as connection:
            return self._execute(connection, script, None, script=True)

    def set_schema(self, schema):
        '''Save the DB schema.'''
        self._db_schema = schema

    def execute_schema(self):
        '''Execute the DB schema.'''
        if not self._db_schema:
            raise DBError('No schema set')

        self.executescript(self._db_schema)

    @staticmethod
    def row2dict(row):
        '''Convert row returned by the underlying lib into a regular dict.'''
        if row:
            # due to psycopg2 returning named tuples, it's easier to just check
            # for namedtuple's _asdict() method instead of findind out what its
            # class is
            if hasattr(row, '_asdict'):
                return row._asdict()

            # sqlite3.Row can be directly cast into a dict
            return dict(row)
        return None

    @classmethod
    def split_statement_values(cls, args, kwargs):
        '''Separate statement from other unnamed arguments and build the values
        tuple or dict for query(), when applicable.'''

        if args:
            statement = args[0]
            args = args[1:]

            # both positional and named args
            if args and kwargs:
                raise ValueError('Positional and named arguments cannot be both '
                                 'provided at the same time.')

            # only positional args
            elif args:
                # if there's only one argument and it is a tuple, list or dict,
                # just pass it along to execute() as is
                if len(args) == 1 and cls._is_values_tuple_or_dict(args[0]):
                    return statement, args[0]
                else:
                    return statement, tuple(args)

            # only named args
            elif kwargs:
                return statement, kwargs

            # no args
            else:
                # note that psycopg2 supports the values parameter being None,
                # but sqlite3 only supports it being an empty list
                return statement, []

        # no statement
        raise DBError('No statement provided')

    @staticmethod
    def _is_values_tuple_or_dict(arg):
        '''Checks that arguments is of a type supported by query().'''
        return isinstance(arg, (tuple, list, dict))

    # convenience methods for building and executing common DB queries

    def select_from(self, table_name, equalities, comparisons=None, columns='*',
                    joins=None, append_to_statement='', one=True):
        '''Select one or more rows from the specified tables, where each row
        matches the values provided.

        WARNING: table_name, append_to_statement and each key and operator must
        always be hardcoded since they are subject to injection attacks.

        See QueryFactory.build_select_query() for usage information.
        '''

        statement, values = QF.build_select_query(
            table_name, equalities, comparisons, columns, joins,
            append_to_statement, self.db_type)

        if self._debug_file:
            print('SQL statement: "{}", Values: {}'.format(
                statement, tuple(values)), file=self._debug_file)

        if one:
            return self.fetchone(statement, values)
        else:
            return self.fetchall(statement, values)

    def insert_into(self, table_name, values_dict, replace=False,
                    upsert_on=None, upsert_values=None, on_conflict=None,
                    returning=None, return_cursor=False):
        '''Insert a new row into a table.

        Note that replace=True is only supported by sqlite while returning is
        only supported by postgres. Also note that upsert is only supported by
        postgres 9.5 or higher and sqlite 3.24 or higher.

        If set, upsert_on should contain the conflict target (e.g. the name of
        the column whose unique constraint is triggering the upsert). If
        upsert_values is not set, values_dict will be used to update all values.

        If returning is set, return the value for that column. If returning is
        None and return_cursor is True, return the cursor that executed the
        query. Otherwise return nothing.

        NOTE that replace, upsert_on and on_conflict are mutually exclusive.

        WARNING: table_name and each key in values_dict must always be hardcoded
        since they are subject to injection attacks.
        '''

        if returning and self.db_type == 'sqlite':
            raise DBError('SQLite does not support RETURNING')

        if replace and self.db_type == 'postgres':
            raise DBError('Postgres does not support REPLACE')

        statement, values = QF.build_insert_query(
            table_name, values_dict, replace=replace, upsert_on=upsert_on,
            upsert_values=upsert_values, on_conflict=on_conflict,
            returning=returning, db_type=self.db_type)

        if self._debug_file:
            print('SQL statement: "{}", Values: {}'.format(
                statement, tuple(values)), file=self._debug_file)

        if returning:
            # note that this might return None if 'ON CONFLICT DO NOTHING' was
            # used
            row = self.fetchone(statement, values)
            return row[returning] if row else None
        else:
            cursor = self.execute(statement, values)
            return cursor if return_cursor else None

    def insert_or_ignore(self, table, values_dict, conflicting_cols='id'):
        '''Insert a row into a table if it does not already exist.

        NOTE that this function is a manual replacement for an ON CONFLICT DO
        NOTHING clause. Callers should consider using that instead whenever it
        is available.

        conflicting_cols should be a column or a list of columns whose values
        determine if the row already exists.

        Returns True if the entry was inserted, False if it already existed.
        '''
        if isinstance(conflicting_cols, str):
            conflicting_cols = [conflicting_cols]

        for col in conflicting_cols:
            if col not in values_dict:
                raise ValueError('Value for column {} not present in '
                                 'values_dict'.format(col))

        unique_values = {
            col: value for col, value in values_dict.items()
            if col in conflicting_cols
        }

        if not self.select_from(table, unique_values, one=True):
            self.insert_into(table, values_dict)
            return True
        return False

    def update(self, table_name, values_dict, equalities, comparisons=None):
        '''Update all matching rows from a table.

        See QueryFactory.build_update_query() for usage information.
        '''

        statement, values = QF.build_update_query(
            table_name, values_dict, equalities, comparisons, self.db_type)

        if self._debug_file:
            print('SQL statement: "{}", Values: {}'.format(
                statement, tuple(values)), file=self._debug_file)

        self.execute(statement, values)

    def delete_from(self, table_name, equalities, comparisons=None):
        '''Delete all matching rows from a table.

        See QueryFactory.build_delete_query() for usage information.
        '''
        statement, values = QF.build_delete_query(
            table_name, equalities, comparisons, self.db_type)

        if self._debug_file:
            print('SQL statement: "{}", Values: {}'.format(
                statement, tuple(values)), file=self._debug_file)

        self.execute(statement, values)
