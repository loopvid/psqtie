#! /usr/bin/env python3
'''Exceptions'''

class DBError(Exception):
    '''An error accessing or modifying the DB'''
    pass
